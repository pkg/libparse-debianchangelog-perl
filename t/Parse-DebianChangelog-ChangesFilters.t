# -*- perl -*-
# Before `make install' is performed this script should be runnable with
# `make test'. After `make install' it should work as `perl Parse-DebianChangelog.t'

#########################

# change 'tests => 1' to 'tests => last_test_to_print';

use strict;
use warnings;

use Test::More tests => 10 + 8;
BEGIN {
    use_ok('Parse::DebianChangelog::ChangesFilters', ':all' );
};

use CGI;

#########################

my $cgi = new CGI;
$cgi->autoEscape(0);

my $text = encode_entities( 'hällö & tschüß <tt id="a">:)</tt>' );

is( $text, 'hällö &amp; tschüß &lt;tt id=&quot;a&quot;&gt;:)&lt;/tt&gt;',
    'encode_entities' );

$text = http_ftp_urls( 'foo http://foo.com/cgi-bin/t.cgi?id=bar&amp;name=bar bar',
		       $cgi );

is( $text,
    'foo <a href="http://foo.com/cgi-bin/t.cgi?id=bar&amp;name=bar">http://foo.com/cgi-bin/t.cgi?id=bar&amp;name=bar</a> bar',
    'http_ftp_urls' );

$text = http_ftp_urls( 'foo &lt;http://foo.com/cgi-bin/t.cgi?id=bar&amp;name=bar&gt; bar',
		       $cgi );

is( $text,
    'foo &lt;<a href="http://foo.com/cgi-bin/t.cgi?id=bar&amp;name=bar">http://foo.com/cgi-bin/t.cgi?id=bar&amp;name=bar</a>&gt; bar',
    'http_ftp_urls with complicated embedding' );

$text = email_to_ddpo( 'Frank Lichtenheld &lt;djpig@debian.org&gt;', $cgi );

is( $text,
    'Frank Lichtenheld &lt;<a href="http://qa.debian.org/developer.php?login=djpig@debian.org">djpig@debian.org</a>&gt;',
    'email_to_ddpo' );

$text = bugs_to_bts( 'This (Closes: #123456, Bug#4321, #222) (hopefully, maybe closes #72345, too)',
		     $cgi );

is( $text,
    'This (Closes: <a class="buglink" href="http://bugs.debian.org/123456">#123456</a>, <a class="buglink" href="http://bugs.debian.org/4321">Bug#4321</a>, <a class="buglink" href="http://bugs.debian.org/222">#222</a>) (hopefully, maybe closes #72345, too)',
    'bugs_to_bts' );

$text = bugs_to_bts( 'This (Closes: 123456, Bug#4321, 222) (hopefully, maybe closes #72345, too)',
		     $cgi );

is( $text,
    'This (Closes: <a class="buglink" href="http://bugs.debian.org/123456">#123456</a>, <a class="buglink" href="http://bugs.debian.org/4321">Bug#4321</a>, <a class="buglink" href="http://bugs.debian.org/222">#222</a>) (hopefully, maybe closes #72345, too)',
    'bugs_to_bts' );

$text = cve_to_mitre( 'SECURITY FIXES (CAN-2005-9999, CVE-2004-9999):',
		      $cgi );

is( $text,
    'SECURITY FIXES (<a href="http://cve.mitre.org/cgi-bin/cvename.cgi?name=CAN-2005-9999">CAN-2005-9999</a>, <a href="http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2004-9999">CVE-2004-9999</a>):',
    'cve_to_mitre' );

$text = pseudo_markup( 'This is a test for *emphasised texts -- and* *more*, we can also make this #really really strong#, #really#',
		       $cgi );

is( $text,
    'This is a test for <em>emphasised texts -- and</em> <em>more</em>, we can also make this <strong>really really strong</strong>, <strong>really</strong>',
    'pseudo_markup' );

my %lics = qw(
  /usr/share/common-licenses/GPL http://www.gnu.org/licenses/gpl.html
  /usr/share/common-licenses/GPL-3 http://www.gnu.org/licenses/gpl.html
  /usr/share/common-licenses/GPL-2 http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
  /usr/share/common-licenses/LGPL http://www.gnu.org/licenses/lgpl.html
  /usr/share/common-licenses/LGPL-2.1 http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
  /usr/share/common-licenses/GFDL http://www.gnu.org/licenses/fdl.html
  /usr/share/common-licenses/GFDL-1.3 http://www.gnu.org/licenses/fdl.html
  /usr/share/common-licenses/GFDL-1.2 http://www.gnu.org/licenses/old-licenses/fdl-1.2.html
);

while( my ($path, $url) = each %lics){
    $text = common_licenses( "And a quick test for $path license texts.",
			     $cgi );
    is( $text,
	qq|And a quick test for <a href="$url">$path</a> license texts.|,
	$path );
}

my $complete_text = <<'EOT';
hällö & tschüß <tt id="a">:)</tt>
foo http://foo.com/cgi-bin/t.cgi?id=bar&name=bar bar
foo <http://foo.com/cgi-bin/t.cgi?id=bar&name=bar> bar
Frank Lichtenheld <djpig@debian.org>
This (Closes: #123456, Bug#4321, 222) (hopefully, maybe closes #72345, too)
SECURITY FIXES (CAN-2005-9999, CVE-2004-9999):
This is a test for *emphasised texts -- and* *more*, we can also make this #really really strong#, #really#
And a quick test for /usr/share/common-licenses/GPL-2 license textes
EOT
    ;

my $complete_result = <<'EOR';
hällö &amp; tschüß &lt;tt id=&quot;a&quot;&gt;:)&lt;/tt&gt;
foo <a href="http://foo.com/cgi-bin/t.cgi?id=bar&amp;name=bar">http://foo.com/cgi-bin/t.cgi?id=bar&amp;name=bar</a> bar
foo &lt;<a href="http://foo.com/cgi-bin/t.cgi?id=bar&amp;name=bar">http://foo.com/cgi-bin/t.cgi?id=bar&amp;name=bar</a>&gt; bar
Frank Lichtenheld &lt;<a href="http://qa.debian.org/developer.php?login=djpig@debian.org">djpig@debian.org</a>&gt;
This (Closes: <a class="buglink" href="http://bugs.debian.org/123456">#123456</a>, <a class="buglink" href="http://bugs.debian.org/4321">Bug#4321</a>, <a class="buglink" href="http://bugs.debian.org/222">#222</a>) (hopefully, maybe closes #72345, too)
SECURITY FIXES (<a href="http://cve.mitre.org/cgi-bin/cvename.cgi?name=CAN-2005-9999">CAN-2005-9999</a>, <a href="http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2004-9999">CVE-2004-9999</a>):
This is a test for <em>emphasised texts -- and</em> <em>more</em>, we can also make this <strong>really really strong</strong>, <strong>really</strong>
And a quick test for <a href="http://www.gnu.org/licenses/old-licenses/gpl-2.0.html">/usr/share/common-licenses/GPL-2</a> license textes
EOR
    ;

$text = Parse::DebianChangelog::ChangesFilters::all_filters( $complete_text,
							     $cgi );

is( $text, $complete_result, 'all_filters' );
